import validateToken, { encode } from '../../src/lib/authorization/validate'

test('Validate Token returns payload when valid', async () => {
    const secret = 'secret'
    const payload = {
        id: '1',
        name: 'John Doe',
        email: 'john.doe@test.com',
        roles: ['admin', 'user'],
        permissions: ['admin:access', 'user:access'],
    }
    const jwt = await encode({ token: payload, secret })

    const result = await validateToken(jwt, secret)

    expect(result).not.toBeNull()
})

describe('Validate Token returns null when invalid', () => {
    test('Token with invalid secret', async () => {
        const secret = 'secret'
        const payload = {
            id: '1',
            name: 'John Doe',
            email: 'john.doe@test.com',
            roles: ['admin', 'user'],
            permissions: ['admin:access', 'user:access'],
        }
        const jwt = await encode({ token: payload, secret })

        const result = await validateToken(jwt, 'inavlid secret')

        expect(result).toBeNull()
    })
})
