FROM node:12 AS base
WORKDIR /src

COPY package*.json yarn.lock ./
RUN yarn install --production
COPY . .

# RUN yarn build
# RUN curl -sSL https://rover.apollo.dev/nix/latest | sh
# RUN rover supergraph compose --config ./supergraph-config.yaml > supergraph.graphql

ENV PORT=4000

CMD [ "yarn", "prod" ]