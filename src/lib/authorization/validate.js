import { EncryptJWT, jwtDecrypt } from 'jose'
import hkdf from '@panva/hkdf'
import { v4 as uuid } from 'uuid'

const DEFAULT_MAX_AGE = 30 * 24 * 60 * 60 // 30 days
const now = () => (Date.now() / 1000) | 0

export default async function validateToken(token, secret = process.env.JWT_SECRET) {
    const result = await decode({ token, secret})
        .then(payload => payload)
        .catch(() => null)
    return result
}

/** added for testing purposes */
export async function encode({ token = {}, secret, maxAge = DEFAULT_MAX_AGE }) {
    const encryptionSecret = await getDerivedEncryptionKey(secret)
    return await new EncryptJWT(token)
        .setProtectedHeader({ alg: 'dir', enc: 'A256GCM' })
        .setIssuedAt()
        .setExpirationTime(now() + maxAge)
        .setJti(uuid())
        .encrypt(encryptionSecret)
}

/** Decodes a NextAuth.js issued JWT. */
export async function decode({ token, secret }) {
    if (!token) return null
    const encryptionSecret = await getDerivedEncryptionKey(secret)
    const { payload } = await jwtDecrypt(token, encryptionSecret, {
        clockTolerance: 15,
    })
    return payload
}

export async function getDerivedEncryptionKey(secret) {
    return await hkdf(
        'sha256',
        secret,
        '',
        'NextAuth.js Generated Encryption Key',
        32
    )
}
