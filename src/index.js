require('dotenv').config()
import { ApolloServer } from 'apollo-server-express'
import { ApolloGateway, RemoteGraphQLDataSource } from '@apollo/gateway'
import { readFileSync } from 'fs'
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core'
import express from 'express'
import http from 'http'
import cookieParser from 'cookie-parser'
import validateToken from './lib/authorization/validate'

const PORT = process.env.PORT || 4000

const allowList = ['http://localhost:3000', 'http://host.docker.internal:3000', 'https://studio.apollographql.com', 'https://s3-pp-front.herokuapp.com', 'http://s3.ryanis.moe' ]
const corsConfig = {
    origin: (origin, callback) => {
        if (!origin || allowList.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    },
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204,
    credentials: true
}

const startApolloServer = async function () {
    const supergraphSdl = readFileSync('./supergraph.graphql').toString()

    const gateway = new ApolloGateway({
        supergraphSdl,
        buildService({ _, url }) {
            return new RemoteGraphQLDataSource({
                url,
                willSendRequest({ request, context }) {
                    // request.http.headers.set('token', context.token)
                    request.http.headers.set('user-id', context.token?.sub)
                }
            })
        }
    })

    const app = express()
    app.use(cookieParser())
    
    const httpServer = http.createServer(app)
    const server = new ApolloServer({
        gateway,
        plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
        context: async ({ req }) => {
            return {
                rawToken: req.cookies['next-auth.session-token'],
                token: await validateToken(req.cookies['next-auth.session-token'])
            }
        }
    })

    await server.start()

    server.applyMiddleware({ app, cors: corsConfig, path: '/' })

    await new Promise(resolve => httpServer.listen({ port: PORT }, resolve))
    console.log(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`)
}
startApolloServer()
